clear-known-hosts:
	@ansible-playbook --inventory=./inventory_local/hosts.ini setup_local_connectivity.yml

local-connect:
	for host in $$(grep ansible_host inventory_local/hosts.ini | tr -s ' ' | cut -d' ' -f2 | cut -d'=' -f2); do \
  		ssh-keygen -R $$host ; \
		ssh_key=$$(grep $$host inventory_local/hosts.ini | tr -s ' ' | cut -d' ' -f4 | cut -d'=' -f2) ; echo $$host; \
		ssh -o IdentitiesOnly=yes -i $$ssh_key vagrant@$$host date ; \
	done
#	for host in $$(grep ansible_host inventory_local/hosts.ini | tr -s ' ' | cut -d' ' -f2 | cut -d'=' -f2); do \
#	  ssh -o IdentitiesOnly=yes -i ~/.ssh/clusters/vagrant/rancher rancher@$$host date ; \
#	done
#	for host in $$(grep ansible_host .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory | tr -s ' ' | cut -d' ' -f2 | cut -d'=' -f2); do \
#	  ssh -o IdentitiesOnly=yes -i ~/.ssh/clusters/vagrant/rancher rancher@$$host date ; \
#	done

setup:
	@ansible-playbook --inventory=./inventory_local/hosts.ini setup.yml

render-rke-config:
	@ansible-playbook --inventory=./inventory_local/hosts.ini setup.yml --tags "render"

patch-namespaces:
	for namespace in $$(kubectl get namespaces -A -o json | jq -r '.items[].metadata.name'); do \
	  kubectl patch serviceaccount default -n $${namespace} -p "$$(cat account_update.yaml)" ; \
	done

annotage-storage-nodes:
	kubectl annotate node node1 'node.longhorn.io/default-disks-config=[{"name":"local-disk","path":"/var/lib/longhorn","allowScheduling":true,"tags":["ssd","fast"]}]'
	kubectl annotate node node2 'node.longhorn.io/default-disks-config=[{"name":"local-disk","path":"/var/lib/longhorn","allowScheduling":true,"tags":["ssd","fast"]}]'
	kubectl annotate node node3 'node.longhorn.io/default-disks-config=[{"name":"local-disk","path":"/var/lib/longhorn","allowScheduling":true,"tags":["ssd","fast"]}]'

	kubectl annotate node node1 'node.longhorn.io/default-node-tags=["fast","storage"]'
	kubectl annotate node node2 'node.longhorn.io/default-node-tags=["fast","storage"]'
	kubectl annotate node node3 'node.longhorn.io/default-node-tags=["fast","storage"]'

apply-global-resources:
	kubectl apply -f https://gitlab.com/FelixZ92/k8s-dev/-/raw/master/00_global-resources/psp/restricted.yaml
	kubectl apply -f https://gitlab.com/FelixZ92/k8s-dev/-/raw/master/00_global-resources/priority-classes.yaml
	kubectl apply -f https://gitlab.com/FelixZ92/k8s-dev/-/raw/master/00_global-resources/namespace.yaml
	for namespace in $$(kubectl get namespaces -A -o json | jq -r '.items[].metadata.name'); do \
      kubectl patch serviceaccount default -n $${namespace} -p "$$(cat account_update.yaml)" ; \
    done
	kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v0.16.1/cert-manager.yaml
	kubectl apply -f https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.12.5/controller.yaml

provsion-kube: spin-up-cluster annotage-storage-nodes apply-global-resources patch-namespaces
